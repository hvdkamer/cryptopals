program hex2base64;

var
  hexFile: file;
  hexChar: byte;
  numRead, totalRead: word;


procedure hexCharWrite;
var
  tempChar: byte;
begin
  tempChar := hexChar;
  if (hexChar <  48) then tempChar := 46;      {ASCII for .}
  if (hexChar >  57) and (hexChar < 65) then tempChar := 46;
  if (hexChar >  70) and (hexChar < 97) then tempChar := 46;
  if (hexChar > 102) then tempChar := 46;
  write(chr(tempChar));
  if (tempChar <> 46) then begin
    inc(totalRead);
    if totalRead mod 6 = 0 then write(' ');
  end; {if}
end;


begin
  assign(hexFile, 'input.hex');
  reset(hexFile, 1);
  totalRead := 0;
  while true do begin
    blockRead(hexFile, hexChar, 1, numRead);
    if numRead = 0 then break;
    hexCharWrite;
  end; {while}
  writeln();
  close(hexFile);
end.
